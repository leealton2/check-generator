# check-gen

_Generate unit tests for c projects via check_

## Dependencies

- [ctags](http://ctags.sourceforge.net)

- [mcookie](https://git.kernel.org/pub/scm/utils/util-linux/util-linux.git/about)

- [mustach](https://gitlab.com/jobol/mustach)

## Installation

```sh
make install
```

## Usage

```sh
check-gen -h
Usage: check-gen [option] ... [directory] ...

Options:
  -h, --help            Show this message and quit
  -d, --debug           Output debugging messages
  -q, --quiet           Only output fatal error messages
  -v, --verbose         Be verbose (show external command output)
  --version             Print version and exit

Arguments:
  directory             Path to project source
```

### Environment Variables

| Environment Variable | Default Value                         |
| -------------------- | ------------------------------------- |
| `TEST_DIR`           | `test`                                |
| `TEMPLATE_DIR`       | `/usr/local/share/check-gen/template` |
| `BASE_TEMPLATE`      | `${TEMPLATE_DIR}/check_test.c`        |

## License

SPDX-License-Identifier: [GPL-2.0-or-later](COPYING)

## Reference

- [Check - Tutorial: Basic Unit Testing](https://libcheck.github.io/check/doc/check_html/check_3.html#Setting-Up-the-Money-Build-Using-Autotools)

