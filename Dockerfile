FROM alpine:edge

RUN apk update && apk add --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing \
      check ctags gcc git json-c-dev make musl-dev mustach shellcheck

WORKDIR /usr/src/check-gen
COPY . /usr/src/check-gen

RUN make install

ENTRYPOINT ["/usr/local/bin/check-gen"]
