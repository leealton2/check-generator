INSTALL = /usr/bin/install
INSTALL_DATA = ${INSTALL} -m 644
SHELLCHECK ?= shellcheck
SHUNIT ?= shunit2

PROGRAM_NAME = check-gen

PREFIX ?= /usr/local
BIN_DIR = ${PREFIX}/bin
SHARE_DIR = ${PREFIX}/share/${PROGRAM_NAME}

TARGET_DIR = .
BUILD_DIR = ${TARGET_DIR}
SRC_DIR = ${TARGET_DIR}/src
TEMPLATE_DIR = ${TARGET_DIR}/template
TEST_DIR = ${TARGET_DIR}/test

SRC = ${SRC_DIR}/generator.sh

TEST_SRC = ${TEST_DIR}/generator.sh

all: ${PROGRAM_NAME}

${PROGRAM_NAME}:
	@cp ${SRC} ${BUILD_DIR}/${PROGRAM_NAME}
	@sed -i 's|TEMPLATE_DIR=".*"|TEMPLATE_DIR="${SHARE_DIR}/template"|g' ${BUILD_DIR}/${PROGRAM_NAME}
	@chmod +x ${BUILD_DIR}/${PROGRAM_NAME}

check:
	@${SHELLCHECK} ${SRC}

test:
	@${SHUNIT} ${TEST_SRC}

install: ${PROGRAM_NAME}
	@${INSTALL} ${BUILD_DIR}/${PROGRAM_NAME} ${BIN_DIR}
	@mkdir -p ${SHARE_DIR}
	@cp -R ${TEMPLATE_DIR} ${SHARE_DIR}/

clean:
	@rm -rf ${BUILD_DIR}/${PROGRAM_NAME}

.PHONY: ${PROGRAM_NAME} all check clean install test
