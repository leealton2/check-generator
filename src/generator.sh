#!/bin/sh
# Generate unit tests for c projects via check
# SPDX-License-Identifier: GPL-2.0-or-later
set -eu

# Constants
VERSION="0.1.1"

# Options
debug=0
verbose=0
quiet=0

usage() {
  msg="${1-}"

  if [ -n "${msg}" ]; then
    printf "%s\n" "${msg}"
  fi
  cat <<EOF
Usage: check-gen [option] ... [directory] ...

Options:
  -h, --help            Show this message and quit
  -d, --debug           Output debugging messages
  -q, --quiet           Only output fatal error messages
  -v, --verbose         Be verbose (show external command output)
  --version             Print version and exit

Arguments:
  directory             Path to project source
EOF
}

check_deps() {
  deps="ctags mcookie mustach"

  for dep in $deps; do
    if [ -z "$(command -v "${dep}")" ]; then
      exit 125
    fi
  done
}

check_env_vars() {
  if [ -z "${TEST_DIR+x}" ]; then
    TEST_DIR="test"
    export TEST_DIR
  fi

  if [ -z "${TEMPLATE_DIR+x}" ]; then
    TEMPLATE_DIR="/usr/local/share/check-gen/template"
    export TEMPLATE_DIR
  fi

  if [ -z "${BASE_TEMPLATE+x}" ]; then
    BASE_TEMPLATE="${TEMPLATE_DIR}/check_test.c"
    export BASE_TEMPLATE
  fi
}

get_sources() {
  find "${1}" -name '*.c'
}

get_functions() {
  ctags -x --c-types=f "$(readlink -f "${1}")" | \
	  awk '/\.c/{ a_len=split($0,a,"/"); b_len=split(a[a_len],b,"[.]c"); } END { for (i in b) if ( i > 1 ) { print b[i] } }'
}

generate_tests() {
  SRC="$(get_sources "${1}")"

  for src in $SRC; do
    functions="$(get_functions "${src}")"
    # shellcheck disable=SC2034
    for func in ${functions}; do
      TEST_JSON="${TMPDIR}/$(mcookie)"
      TEST_NAME="$(basename "${src}" | awk '{ split($0,a,"[.]c"); print a[1] }')"
      echo "{ \"test_name\": \"${TEST_NAME}\" }" > "${TEST_JSON}"
      mustach "${TEST_JSON}" "${BASE_TEMPLATE}" > "${1}/${TEST_DIR}/check_${TEST_NAME}.c"
    done
  done
}

main() {
  # Parse args
  while [ ${#} -gt 0 ]
  do
    a=${1}
    shift
    case "${a}" in
      -h|--help)
        usage
        exit 0
        ;;
      -d|--debug)
        debug=1
        ;;
      -q|--quiet)
        if [ ${verbose} = 1 ]; then
          usage "The --quiet and --verbose options are mutually exclusive"
          exit 1
        fi
        quiet=1
        ;;
      -v|--verbose)
        if [ ${quiet} = 1 ]; then
          usage "The --quiet and --verbose options are mutually exclusive"
          exit 1
        fi
        verbose=1
        ;;
      --version)
        echo "Check generator version ${VERSION}"
        exit 0
        ;;
      -*)
        usage "You have specified an invalid option: ${a}"
        exit 1
        ;;
      *)
        src_dir=$a
        ;;
    esac
  done

  check_deps
  check_env_vars

  generate_tests "${src_dir}"
}

if [ "$(basename "${0}")" = "check-gen" ]; then
  main "${@}"
fi
